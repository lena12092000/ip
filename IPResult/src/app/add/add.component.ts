import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ListService } from '../shared/services/list.service';
import { Router, ActivatedRoute } from '@angular/router';
import { isNullOrUndefined } from 'util';



@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {
  FormAdd:FormGroup;
  nowDate = new Date();
  disabled = true;
  

  constructor(
    private listService:ListService,
    private router: Router
    ) { 
    

  }



  ngOnInit() {
    
    this.FormAdd = new FormGroup({
      name: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      teg:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      lenguage: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      connectService: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      responsible: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      document: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      repozit: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      status: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      content:new FormControl({value:"", disabled:this.disabled}, Validators.required)
    });
  }


   
  async add(name, teg, lenguage, connectService, responsible, document, repozit, status, content) { 
    
    try {
       await this.listService.postNotes(
        {
          "name":name,
          "status":status,
          "teg":teg,
          "lenguage":lenguage,
          "connectService":connectService,
          "responsible":responsible,
          "document":document,
          "repozit":repozit,
          "content":content

        }
      );
      this.router.navigate(['/list']);
    } catch(e){
      console.error(e);
    }
  }


}
