import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import { EditComponent } from './edit/edit.component';
import { ContactsComponent } from './contacts/contacts.component';
import { SchemaComponent } from './schema/schema.component';
import { MainComponent } from './main/main.component';
import { AutorizationComponent } from './autorization/autorization.component';
import { WorkerComponent } from './worker/worker.component';


const routes: Routes = [
  {path: '', component:MainComponent},
  {path:'list', component:ListComponent},
  {path:'add', component:AddComponent},
  { path: 'edit/:id', component: EditComponent },
  { path: 'contacts', component: ContactsComponent },
  { path: 'schema', component: SchemaComponent},
  {path: 'auto', component:AutorizationComponent}, 
  {path:'worker', component:WorkerComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
