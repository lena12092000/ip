import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpClientModule} from "@angular/common/http";
import { AppRoutingModule  } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './header/header.component';
import { ListComponent } from './list/list.component';
import { AddComponent } from './add/add.component';
import {ReactiveFormsModule, FormsModule} from '@angular/forms';
import { TextMaskModule } from 'angular2-text-mask';
import { EditComponent } from './edit/edit.component';
import { FilterPipe } from './shared/pipes/filter.pipe';
import { ContactsComponent } from './contacts/contacts.component';
import { SortPipe } from './shared/pipes/sort.pipe';
import { SchemaComponent } from './schema/schema.component';
import { Sort2Pipe } from './shared/pipes/sort2.pipe';
import { Sort3Pipe } from './shared/pipes/sort3.pipe';
import { MainComponent } from './main/main.component';
import { AutorizationComponent } from './autorization/autorization.component';
import { HeaderOneComponent } from './header-one/header-one.component';
import { FooterComponent } from './footer/footer.component';
import { WorkerComponent } from './worker/worker.component';


@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ListComponent,
    AddComponent,
    EditComponent,
    FilterPipe,
    ContactsComponent,
    SortPipe,
    SchemaComponent,
    Sort2Pipe,
    Sort3Pipe,
    MainComponent,
    AutorizationComponent,
    HeaderOneComponent,
    FooterComponent,
    WorkerComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    TextMaskModule,
    FormsModule
  


  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
