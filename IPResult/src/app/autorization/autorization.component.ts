import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-autorization',
  templateUrl: './autorization.component.html',
  styleUrls: ['./autorization.component.css']
})
export class AutorizationComponent implements OnInit {
  FormAuto:FormGroup;
  disabled = false;
  constructor() { }

  ngOnInit() {
    this.FormAuto = new FormGroup({
      email: new FormControl({value:"", disabled:this.disabled}, [Validators.required, Validators.email]),
      pass:new FormControl({value:"", disabled:this.disabled}, [Validators.required])
    });
  }

}
