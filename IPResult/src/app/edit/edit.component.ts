import { Component, OnInit, Input } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { ListService } from '../shared/services/list.service';
import { list } from '../shared/modules/list/list.module';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.css']
})
export class EditComponent implements OnInit {
  FormEdit:FormGroup;
  disabled = true;
  id:number;
  list;
  addEdit: string = "Изменить";
  isEdit: boolean;


  constructor(private activeRoute: ActivatedRoute,
    private listService: ListService,
    private router:Router
    ) { 
    this.activeRoute.params.subscribe(param => {
      this.id = param.id;
      
    })
  }

  ngOnInit() {
    this.getList().then(() => {
    this.FormEdit = new FormGroup({
      name: new FormControl({value:"", disabled:this.disabled}, Validators.required),
      teg:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      status:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      lenguage:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      connectService:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      responsible:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      document:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      repozit:new FormControl({value:"", disabled:this.disabled}, Validators.required),
      content:new FormControl({value:"", disabled:this.disabled}, Validators.required),

    });
  })
}

async getList(){
  
  try{
    this.list = await this.listService.getById(this.id)
  }catch(e){
    console.log(e);
  }
}

async edit(name, teg, status, lenguage, connectService, responsible, document, repozit, content){
  try{
    await this.listService.putById(
      this.id,
      {
        name:name,
        teg:teg,
        status:status,
        lenguage:lenguage,
        connectService:connectService,
        responsible:responsible,
        document:document,
        repozit:repozit,
        content:content
      }
      );
    this.router.navigate(['/list']);
  }catch(e){
    console.log(e);
  }
}
showEdit() {
  this.isEdit = !this.isEdit;
  if(this.addEdit == "Изменить") {
    this.addEdit = "Сохранить";
  } else {
    this.addEdit = "Изменить";
  }
}


async delete(id:number) {
  try
  {
    await this.listService.deleteById(id);
    this.router.navigate(['/list']);
  
   
  } catch (e)
  {
    console.log(e);
  
  }
}

  public mask = [/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,'.',/[0-9]/,/[0-9]/,/[0-9]/,/[0-9]/]
}
