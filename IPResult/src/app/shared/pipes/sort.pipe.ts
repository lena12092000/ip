import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {
  

  transform(lists,  sort='every' ) {
    if(!isNullOrUndefined(lists) && ((sort).trim()) !== ""){
      if (sort == 'every'){
        return lists;
      }
      if (sort == 'work'){
        let list = lists.filter(
          lists => lists.status === "Запущен"
        );
        return list;
      }
      else if(sort == 'notwork'){
        let list = lists.filter(
          lists => lists.status === "В разработке"
        );
        return list;
      }
    }

}
}
