import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'sort2'
})
export class Sort2Pipe implements PipeTransform {

  transform(lists,  sort='every' ) {
    if(!isNullOrUndefined(lists) && ((sort).trim()) !== ""){
      if (sort == 'every'){
        return lists;
      }
      else if(sort == 'java'){
        let list = lists.filter(
          lists => lists.lenguage === "Java"
        );
        return list;
      }
      else if(sort == 'php'){
        let list = lists.filter(
          lists => lists.lenguage === "PHP"
        );
        return list;
      }
    }

}
}

