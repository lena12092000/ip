import { Pipe, PipeTransform } from '@angular/core';
import { isNullOrUndefined } from 'util';

@Pipe({
  name: 'sort3'
})
export class Sort3Pipe implements PipeTransform {

  transform(lists,  sort='every' ) {
    if(!isNullOrUndefined(lists) && ((sort).trim()) !== ""){
      if (sort == 'every'){
        return lists;
      }
      else if(sort == 'name1'){
        let list = lists.filter(
          lists => lists.responsible === "name1"
        );
        return list;
      } else if(sort == 'name2'){
        let list = lists.filter(
          lists => lists.responsible === "name2"
        );
        return list;
      }
    }

}
}

